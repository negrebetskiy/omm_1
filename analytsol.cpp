#include <iostream>
#include <cmath>
#include <vector>
#include "omm1.h"
using namespace std;

struct p_u {
	x_t p;
	double u;
};

// my equation ...
double F(x_t point, double u) {
	return 2.0*point.t*u*exp(-u*u) + (2.0/M_PI)*asin(u) - point.x;
}
//... and it's partial drivative
double Fu(x_t point, double u) {
        return -4.0*point.t*exp(-u*u)*u*u + 2.0*point.t*exp(-u*u) + (2.0/M_PI)*pow(1.0-u*u,-0.5);
}

int main() {
	vector<p_u> stored;
	double u, delta;
	double mindelta = pow(10, 5);	
	double u_approx = 0.0;
	x_t p = {0, 0};
	p_u buf = {p, u_approx};
	stored.push_back(buf);
	

	for (double x=0.0; x<=1.0; x = x+0.01) {
		for (double t=0.0; t<=1.0; t = t+0.01) {
			p.x = x;
			p.t = t;
			
			/*for (vector<p_u>::iterator it = stored.begin(); it != stored.end(); ++it) {
				buf = *it;
				delta = sqrt(pow(buf.p.x-p.x, 2) + pow(buf.p.t-p.t, 2));
				if (mindelta > delta) {
					mindelta = delta;
					u_approx = buf.u;
				}
				mindelta = pow(10, 5);
			}*/

			u = NeutSolver(F, Fu, eps, p, u_approx);
			if (isnan(F(p, u))) {
				u_approx = 0.999999;
				u = NeutSolver(F, Fu, eps, p, u_approx);
			}
			cout <<  x << " " << t << " " << u << endl;
			//buf = {p, u};
			//stored.push_back(buf);
		}
	}
}