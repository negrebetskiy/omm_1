set terminal pdf
set output 'report-gnuplottex-fig1.pdf'
   f(t, x0) = x0 + 2*sin(pi*x0/2)*exp(-(sin(pi*x0/2))**2)*t
unset key
set xrange[0:1]
set yrange[0:1]
set xlabel "t"
set ylabel "x"
set style line 1 linecolor rgb "blue"
plot for [x0=0:20] f(t=x, x0/20.0) with lines ls 1
