set terminal pdf
set output 'report-gnuplottex-fig2.pdf'
set style line 1 linecolor rgb "blue"
set xlabel "x"
set ylabel "t"
set zlabel "u"
unset key
splot '/home/vasily/code/omm_1/data.txt' with lines ls 1
