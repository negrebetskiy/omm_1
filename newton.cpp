#include <iostream>
#include <cmath>
#include "omm1.h"
using namespace std;

// Realization of Newton-Rapson method lol

double NeutSolver(double (*f)(x_t, double), double (*fu)(x_t, double),
		  double max_err, x_t point, double u_approx) {
	double u = u_approx;
	while (fabs(f(point, u))>max_err) {
		u = u - f(point, u)/fu(point, u);
	}
	return u;
}
