#ifndef OMM1_H
#define OMM1_H

static double eps =  pow(10.0, -5.0)
;
struct x_t {
	double x;
	double t;
};

double NeutSolver(double (*f)(x_t, double), double (*fu)(x_t, double),
		  double max_err, x_t point, double u_approx);
#endif
