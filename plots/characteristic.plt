f(t, x0) = x0 + 2*sin(pi*x0/2)*exp(-(sin(pi*x0/2))**2)*t
unset key
set xrange[0:6]
set yrange[0:8]
set style line 1 linecolor rgb "blue"
plot for [x0=-28:28] f(t=x, x0/4.0) with lines ls 1
